const moment = require('moment');
const database = require('database');

const OUTDATED_ERROR = 'OUTDATED_ERROR';
const NIVELAMENTO_NAME = 'AUTNIV';

const QSearchConfig = '\
SELECT \
    e.id, \
    DATE_FORMAT(e.start_date, \'%Y-%m-%dT%TZ\') as startDate, \
    DATE_FORMAT(e.end_date, \'%Y-%m-%dT%TZ\') as endDate, \
    CASE WHEN se.id IS NULL THEN e.description ELSE se.name END as description, \
    CASE WHEN se.id IS NULL THEN e.score_limit ELSE se.score_limit END as score, \
    e.eventId as eventType,\
    et.name as eventTypeName, \
    se.id as subEventId \
  FROM sp_event_config e \
    INNER JOIN sp_event_type et ON (e.eventId = et.id) \
    LEFT JOIN sp_sub_event se ON (e.id = se.eventconfigid) \
  WHERE ? IN (e.event_alias, se.sub_event_alias)';

const QSearchConfigByName = '\
SELECT \
  ec.id as id, \
  ec.score_limit as scoreLimit, \
  DATE_FORMAT(ec.start_date, \'%Y-%m-%dT%TZ\') as startDate, \
  DATE_FORMAT(ec.end_date, \'%Y-%m-%dT%TZ\') as endDate, \
  ec.description, \
  ec.eventId as eventType, \
  dr.id disciplineRule, \
  sr.id studentRule, \
  es.score as score \
FROM sp_student s \
  INNER JOIN sp_student_rules sr ON (s.id = sr.student_id) \
  INNER JOIN sp_discipline_rules dr ON (sr.disciplineruleid = dr.id) \
  INNER JOIN sp_discipline d ON (dr.disciplineid = d.id) \
  INNER JOIN sp_score_rules rules ON (dr.scoreruleid = rules.id) \
  INNER JOIN sp_event_config ec ON (rules.id = ec.ruleid) \
  INNER JOIN sp_event_type et ON (ec.eventid = et.id) \
  INNER JOIN sp_event_scores es ON (ec.id = es.eventconfigid) \
WHERE \
  dr.semester = ? \
  AND dr.active = 1 \
  AND et.name = ? \
  AND s.id = ? \
  AND es.type = \'PERFORM\' ';

/**
* Gets the EventConfig by name
*/
const getConfigsByName = (conn, semester, studentId) => {
  return new Promise((resolve, reject) => {
    conn.query(QSearchConfigByName, [semester, NIVELAMENTO_NAME, studentId], (err, result) => {
      if (err) { reject(err); }
      return resolve(result);
    });
  });
};


/**
 * Gets the EventConfig by its alias
 */
const getEventConfig = (conn, eventAlias) => {
  return new Promise((resolve, reject) => {
    conn.query(QSearchConfig, [eventAlias], (err, result) => {
      if (err) { reject(err); }
      if (result.length) { resolve(result[0]); }

      return reject(database.buildEntityNotFoundError('sp_event_config', 'event_alias', eventAlias));
    });
  });
};

/**
 * Builds OutDate Error
 */
const buildOutDateError = (startTime, endTime, eventTime) => {
  const startDate = moment(startTime).toISOString(true);
  const endDate = moment(endTime).toISOString(true);
  const message = `Event is outdated, it should be between ${startDate} - ${endDate} and it was ${eventTime}`;
  const error = new Error(message);
  error.type = OUTDATED_ERROR;
  return error;
};

/**
 * Checks if the eventTime is between the startDate and endDate of the eventconfig
 */
const isDateValid = (config, eventDate) => {
  const eventTime = moment(eventDate).utc();
  const startTime = moment.utc(config.startDate);
  const endTime = moment.utc(config.endDate).milliseconds(999);

  if (eventTime.isSameOrAfter(startTime) && eventTime.isSameOrBefore(endTime)) {
    return { success: true };
  }

  return { success: false, error: buildOutDateError(config.startDate, config.endDate, eventDate) };
};

/**
 * Validates a specifig event, checking if it is between the acceptable date.
 * If the event is valid, the event data is return { id, score, startDate, EndDate }
 * Otherwise an error (OUTDATED_ERROR) is emitted
 */
const checkEvent = (eventAlias, eventDate) => {
  return new Promise((resolve, reject) => {
    let conn = null;

    database.getConnection()
      .then((connection) => {
        conn = connection;
        return getEventConfig(conn, eventAlias);
      })
      .then((config) => {
        const result = isDateValid(config, eventDate);
        if (!result.success) {
          reject(result.error);
        }

        return resolve(config);
      })
      .catch((err) => reject(err))
      .then(() => {
        if (conn) {
          conn.release();
        }
      });
  });
};

/**
 * Query an event by its alias
 */
const getEvent = (eventAlias) => {
  return new Promise((resolve, reject) => {
    let conn = null;

    database.getConnection()
      .then((connection) => {
        conn = connection;
        return getEventConfig(conn, eventAlias);
      })
      .then((config) => {
        return resolve(config);
      })
      .catch((err) => reject(err))
      .then(() => {
        if (conn) {
          conn.release();
        }
      });
  });
};

const getAllDisciplinesNivEvents = (studentId, semester, eventDate) => {
  return new Promise((resolve, reject) => {
    let conn = null;

    database.getConnection()
      .then((connection) => {
        conn = connection;
        return getConfigsByName(conn, semester, studentId);
      })
      .then((events) => {
        const result = events.filter((e) => isDateValid(e, eventDate).success);
        return resolve(result);
      })
      .catch((err) => reject(err))
      .then(() => {
        if (conn) {
          conn.release();
        }
      });
  });
};

module.exports.checkEvent = checkEvent;
module.exports.getEvent = getEvent;
module.exports.getAllDisciplinesNivEvents = getAllDisciplinesNivEvents;

module.exports.OUTDATED_ERROR = OUTDATED_ERROR;
